import './style.css'

import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

var activeOption = 'Back';

const TRAY = document.getElementById('js-tray-slide');

const colors = [{
        texture: './assets/fabrics/denim.png',
        size: [2, 2, 2],
        shininess: 60
    },
    {
        texture: './assets/fabrics/fabric1.png',
        size: [3, 3, 3],
        shininess: 0
    },
    {
        texture: './assets/fabrics/fabric2.png',
        size: [3, 3, 3],
        shininess: 0
    },
    {
        texture: './assets/fabrics/leather1.png',
        size: [3, 3, 3],
        shininess: 0
    },
    {
        texture: './assets/fabrics/leather2.png',
        size: [3, 3, 3],
        shininess: 0
    },
    {
        texture: './assets/fabrics/wood.png',
        size: [3, 3, 3],
        shininess: 0
    },
    {
        color: '131417'
    },
    {
        color: '374047'
    },
    {
        color: '5f6e78'
    },
    {
        color: '7f8a93'
    },
    {
        color: '97a1a7'
    },
    {
        color: 'acb4b9'
    },
    {
        color: 'DF9998',
    },
    {
        color: '7C6862'
    },
    {
        color: 'A3AB84'
    },
    {
        color: 'D6CCB1'
    },
    {
        color: 'F8D5C4'
    },
    {
        color: 'A3AE99'
    },
    {
        color: 'EFF2F2'
    },
    {
        color: 'B0C5C1'
    },
    {
        color: '8B8C8C'
    },
    {
        color: '565F59'
    },
    {
        color: 'CB304A'
    },
    {
        color: 'FED7C8'
    },
    {
        color: 'C7BDBD'
    },
    {
        color: '3DCBBE'
    },
    {
        color: '264B4F'
    },
    {
        color: '389389'
    },
    {
        color: '85BEAE'
    },
    {
        color: 'F2DABA'
    },
    {
        color: 'F2A97F'
    },
    {
        color: 'D85F52'
    },
    {
        color: 'D92E37'
    },
    {
        color: 'FC9736'
    },
    {
        color: 'F7BD69'
    },
    {
        color: 'A4D09C'
    },
    {
        color: '4C8A67'
    },
    {
        color: '25608A'
    },
    {
        color: '75C8C6'
    },
    {
        color: 'F5E4B7'
    },
    {
        color: 'E69041'
    },
    {
        color: 'E56013'
    },
    {
        color: '11101D'
    },
    {
        color: '630609'
    },
    {
        color: 'C9240E'
    },
    {
        color: 'EC4B17'
    },
    {
        color: '281A1C'
    },
    {
        color: '4F556F'
    },
    {
        color: '64739B'
    },
    {
        color: 'CDBAC7'
    },
    {
        color: '946F43'
    },
    {
        color: '66533C'
    },
    {
        color: '173A2F'
    },
    {
        color: '153944'
    },
    {
        color: '27548D'
    },
    {
        color: '438AAC'
    }

]

//variabila care spune camerei cat de departe sa se deplaseze fata de punctul initial (0,0,0),
//astfel incat canapeaua sa fie vizibila
var cameraFar = 5;

//modelul 3D, canapeaua
var theModel;
const MODEL_PATH = "./couch2.glb";

//culoarea de fundal
const BACKGROUND_COLOR = 0xffffff;

//initializare scena
const scene = new THREE.Scene();

//setare culoare de fundal si ceata, care ajuta la ascunderea amrginilor podelei
scene.background = new THREE.Color(BACKGROUND_COLOR);
scene.fog = new THREE.Fog(BACKGROUND_COLOR, 20, 100);

const canvas = document.querySelector('#canvas');

//initializare WebGLRenderer, c[ruia ii transmit canvas-ul
//antialias: true = margini mai fin in jurul modelului 3D
const renderer = new THREE.WebGLRenderer({ canvas, antialias: true });

renderer.shadowMap.enabled = true;
renderer.setPixelRatio(window.devicePixelRatio);

document.body.appendChild(renderer.domElement);

//adaugare camera de tip PerspectiveCamera. Campul vizual este de 50, dimensiunea intregii ferestre este calculata automat
//celelalte valori spun cat de departe sau de aproape sa fie camera inainte ca obiectul sa fie redat
var camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);
camera.position.z = cameraFar;
camera.position.x = 0;

//initializare obiect Loader, care ajuta la incarcarea modelului 3D
const loader = new GLTFLoader();

//apelam metoda load
//primul parametru: calea fisierului unde se gaseste modelul
//al doilea parametru: functie ce ruleaza odata ce resursa este incarcata
//al treilea parametru: este undefined, dar paote fi o functie care ruleaza in timp ce resursa se incarca
//ultimul parametru: gestioneaza erorile
loader.load(MODEL_PATH, function(gltf) {
    theModel = gltf.scene;

    theModel.traverse((o) => {
        if (o.isMesh) {
            o.castShadow = true;
            o.receiveShadow = true;
        }
    });

    //scala intitiala a modelului
    theModel.scale.set(2, 2, 2);

    //intoarcere model
    theModel.rotation.y = 2 * Math.PI;

    //coborare model in cadru
    theModel.position.y = -1;

    for (let object of INITIAL_MAP) {
        initColor(theModel, object.childID, object.mtl);
    }

    //adaugare model in scena
    scene.add(theModel);

}, undefined, function(error) {
    console.error(error)
});

//adaugare material alb modleului
const INITIAL_MTL = new THREE.MeshPhongMaterial({ color: 0xf1f1f1, shininess: 10 });

const INITIAL_MAP = [
    { childID: "Back", mtl: INITIAL_MTL },
    { childID: "CushionLeft", mtl: INITIAL_MTL },
    { childID: "CushionMiddle", mtl: INITIAL_MTL },
    { childID: "CushionRight", mtl: INITIAL_MTL },
    { childID: "CushionSeatsLeft", mtl: INITIAL_MTL },
    { childID: "CushionSeatsMiddle", mtl: INITIAL_MTL },
    { childID: "CushionSeatsRight", mtl: INITIAL_MTL },
    { childID: "CushionSideLeft", mtl: INITIAL_MTL },
    { childID: "CushionSideRight", mtl: INITIAL_MTL },
    { childID: "Base1", mtl: INITIAL_MTL },
    { childID: "Base2", mtl: INITIAL_MTL },
    { childID: "Legs", mtl: INITIAL_MTL },
];

//adaugare texturi/culori modelului
function initColor(parent, type, mtl) {
    parent.traverse((o) => {
        if (o.isMesh) {
            if (o.name.includes(type)) {
                o.material = mtl;
                o.nameID = type;
            }
        }
    });
}

//adaugare lumini de tip HemisphereLight
var hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.61);
hemiLight.position.set(0, 50, 0);
scene.add(hemiLight);


//adaugare lumini de tip DirectionalLight
var dirLight = new THREE.DirectionalLight(0xffffff, 0.54);
dirLight.position.set(-8, 12, 8);
dirLight.castShadow = true;
dirLight.shadow.mapSize = new THREE.Vector2(1024, 1024);
scene.add(dirLight);

var floorGeometry = new THREE.PlaneGeometry(5000, 5000, 1, 1);
var floorMaterial = new THREE.MeshPhongMaterial({
    color: 0xeeeeee,
    shininess: 0
});

//floor
var floor = new THREE.Mesh(floorGeometry, floorMaterial);
floor.rotation.x = -0.5 * Math.PI;
floor.receiveShadow = true;
floor.position.y = -1;
scene.add(floor);

var controls = new OrbitControls(camera, renderer.domElement);
controls.maxPolarAngle = Math.PI / 2;
controls.minPolarAngle = Math.PI / 3;
controls.enableDamping = true;
controls.enablePan = false;
controls.dampingFactor = 0.1;
controls.autoRotate = false;
controls.autoRotateSpeed = 0.2;

//Three.js are nevoie de un update loop, adica de o functie care ruleaza pe fiecare extragere de cadre si este foarte importanta
//pentru modul in care functioneaza aplicatia
function animate() {
    controls.update();

    renderer.render(scene, camera);
    requestAnimationFrame(animate);

    if (resizeRendererToDisplaySize(renderer)) {
        const canvas = renderer.domElement;
        camera.aspect = canvas.clientWidth / canvas.clientHeight;
        camera.updateProjectionMatrix();
    }
}

animate();

//aceasta functie 'asculta' atat dimensiunea panzei, cat si dimensiunea ferestrei si returneaza un boolean, in fucntie de
//faptul daca cele doua dimensiuni sunt sau nu egale
//scopul este ca modelul sa se redimensioneze pe orice dimensiune de ecran
function resizeRendererToDisplaySize(renderer) {
    const canvas = renderer.domElement;
    var width = window.innerWidth;
    var height = window.innerHeight;
    var canvasPixelWidth = canvas.width / window.devicePixelRatio;
    var canvasPixelHeight = canvas.height / window.devicePixelRatio;

    const needResize = canvasPixelWidth !== width || canvasPixelHeight !== height;
    if (needResize) {

        renderer.setSize(width, height, false);
    }
    return needResize;
}

//adaugare culori in interfata
function buildColors(colors) {
    for (let [i, color] of colors.entries()) {
        let swatch = document.createElement('div');
        swatch.classList.add('tray__swatch');

        if (color.texture) {
            swatch.style.backgroundImage = "url(" + color.texture + ")";
        } else {
            swatch.style.background = "#" + color.color;
        }


        swatch.setAttribute('data-key', i);
        TRAY.append(swatch);
    }
}

buildColors(colors);

const swatches = document.querySelectorAll(".tray__swatch");

for (const swatch of swatches) {
    swatch.addEventListener('click', selectSwatch);
}

//gestionare selectare optiuni de culaore/textura
function selectSwatch(e) {
    let color = colors[parseInt(e.target.dataset.key)];
    let new_mtl;

    if (color.texture) {

        let txt = new THREE.TextureLoader().load(color.texture);

        txt.repeat.set(color.size[0], color.size[1], color.size[2]);
        txt.wrapS = THREE.RepeatWrapping;
        txt.wrapT = THREE.RepeatWrapping;

        new_mtl = new THREE.MeshPhongMaterial({
            map: txt,
            shininess: color.shininess ? color.shininess : 10
        });
    } else {
        new_mtl = new THREE.MeshPhongMaterial({
            color: parseInt('0x' + color.color),
            shininess: color.shininess ? color.shininess : 10

        });
    }

    setMaterial(theModel, activeOption, new_mtl);

}

//setare material/culoare pe partile canapelei
function setMaterial(parent, type, mtl) {
    parent.traverse((o) => {
        if (o.isMesh && o.nameID != null) {
            if (o.nameID == type) {
                o.material = mtl;
            }
        }
    });
}

const options = document.querySelectorAll(".option");

for (const option of options) {
    option.addEventListener('click', selectOption);
}

function selectOption(e) {
    let option = e.target;
    activeOption = e.target.dataset.option;
    for (const otherOption of options) {
        otherOption.classList.remove('--is-active');
    }
    option.classList.add('--is-active');
}

var slider = document.getElementById('js-tray'),
    sliderItems = document.getElementById('js-tray-slide'),
    difference;

function slide(wrapper, items) {
    var posX1 = 0,
        posX2 = 0,
        posInitial,
        threshold = 20,
        posFinal,
        slides = items.getElementsByClassName('tray__swatch');


    items.onmousedown = dragStart;

    items.addEventListener('touchstart', dragStart);
    items.addEventListener('touchend', dragEnd);
    items.addEventListener('touchmove', dragAction);


    function dragStart(e) {
        e = e || window.event;
        posInitial = items.offsetLeft;
        difference = sliderItems.offsetWidth - slider.offsetWidth;
        difference = difference * -1;

        if (e.type == 'touchstart') {
            posX1 = e.touches[0].clientX;
        } else {
            posX1 = e.clientX;
            document.onmouseup = dragEnd;
            document.onmousemove = dragAction;
        }
    }

    function dragAction(e) {
        e = e || window.event;

        if (e.type == 'touchmove') {
            posX2 = posX1 - e.touches[0].clientX;
            posX1 = e.touches[0].clientX;
        } else {
            posX2 = posX1 - e.clientX;
            posX1 = e.clientX;
        }

        if (items.offsetLeft - posX2 <= 0 && items.offsetLeft - posX2 >= difference) {
            items.style.left = (items.offsetLeft - posX2) + "px";
        }
    }

    function dragEnd(e) {
        posFinal = items.offsetLeft;
        if (posFinal - posInitial < -threshold) {} else if (posFinal - posInitial > threshold) {

        } else {
            items.style.left = (posInitial) + "px";
        }

        document.onmouseup = null;
        document.onmousemove = null;
    }

}

slide(slider, sliderItems);